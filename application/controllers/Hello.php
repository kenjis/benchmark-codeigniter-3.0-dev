<?php

class Hello extends CI_Controller {
	public function index()
	{
		$name = $this->input->get('name') ?: 'World';
		echo 'Hello ' . $name . '!';
	}
}
