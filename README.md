# CodeIgniter 3.0-dev3 for Benchmarking

## Installation

~~~
$ mkdir ~/tmp
$ cd ~/tmp
$ git clone git@bitbucket.org:kenjis/benchmark-codeigniter-3.0-dev.git
~~~

~~~
$ cd benchmark-codeigniter-3.0-dev
$ chmod o+w application/cache application/logs
~~~

~~~
$ cd /path/to/htdocs
$ ln -s ~/tmp/benchmark-codeigniter-3.0-dev ci30
~~~

## Benchmarking

~~~
$ siege -b -c 10 -t 3S http://localhost/ci30/hello?name=BEAR
~~~
